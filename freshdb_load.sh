#!/bin/bash

<<info
Script to Load and backup FRESH db data
usage:
	./freshdb_load.sh -f <sql_dump_path> -i <running_db_container_id>
info


DUMP_FILE_PATH=""
FRESH_DB_CONTAINER_ID=""
FRESH_DB_DATA_VOLUME="fresh-teams_dbdata"
FRESH_DB_NAME="fresh-artichoke"
BACKUP_DIR="fresh_dbbackup"

while getopts ":f:i:" opt; do
	case $opt in
		f) DUMP_FILE_PATH=${OPTARG};;
		i) FRESH_DB_CONTAINER_ID=${OPTARG} ;;
		*) echo "invalid option";;
	esac
done

while true; do
    read -p "$(tput setaf 2) Do you want to backup current DB data [Y/N] $(tput setaf 7) " yn
    case $yn in
        [Yy]* )
				echo "backing database"
				# create backup directory
				if [ ! -d "$BACKUP_DIR" ]
				then
						mkdir ./$BACKUP_DIR
				fi
			 	docker exec -t --user postgres ${FRESH_DB_CONTAINER_ID} pg_dump ${FRESH_DB_NAME} -c -U postgres > ./$BACKUP_DIR/freshdb_dump_`date +%Y%m%d%H%M%S`.sql || exit 1
				break ;;
        [Nn]* ) break;;
        * ) exit 0 ;;
    esac
done


# Check the valid file path
if ! [[ -f "$DUMP_FILE_PATH" ]]; then
		echo "Invalid file path"
		exit 1
fi

# Stop and restart db container to remove all connection from postgres db to DROP database.
# TODO: Find way to remove all connection from postgres session and drop the database
docker container stop ${FRESH_DB_CONTAINER_ID}
docker container restart ${FRESH_DB_CONTAINER_ID}

# # Drop the database
docker exec -it $FRESH_DB_CONTAINER_ID psql -U postgres -d postgres -c 'DROP DATABASE IF EXISTS "fresh-artichoke";' || exit 1
# # Recreate the database
docker exec -it $FRESH_DB_CONTAINER_ID psql -U postgres -d postgres -c 'CREATE DATABASE "fresh-artichoke";' || exit 1

# Load sql dump file
docker cp $DUMP_FILE_PATH $FRESH_DB_CONTAINER_ID:/latest.sql || exit 1
docker exec -u postgres $FRESH_DB_CONTAINER_ID psql fresh-artichoke postgres -f /latest.sql || exit 1

echo "Database dump loaded"

#Fix knex_migrations_id_seq conflict
docker exec -it $FRESH_DB_CONTAINER_ID psql fresh-artichoke postgres -c "SELECT setval('knex_migrations_id_seq', data.count + 1) FROM ( SELECT id as count FROM knex_migrations ORDER BY id DESC LIMIT 1) as data;"
#Fix undo_transactions_id_seq conflict
docker exec -it $FRESH_DB_CONTAINER_ID psql fresh-artichoke postgres -c "SELECT setval('undo_transactions_id_seq', data.count + 1) FROM ( SELECT id as count FROM undo_transactions ORDER BY id DESC LIMIT 1) as data;"


#TODO: ADD option to run latest migration
echo "Make sure you run latest migration"
