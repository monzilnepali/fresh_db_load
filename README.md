Script to backup and restore the database

prerequisite:
 - Make sure db container is running

```sh
chmod +x ./fresh_db_load.sh
./fresh_db_load -f <file_path> -i <container_id>
```

TODO:
1. Add migration option
2. Find way to drop active connection with db123
